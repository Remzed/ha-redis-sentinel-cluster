#!/bin/bash

if [ $# -eq 0 ]; then echo "Need to choose master or slave " ; exit 1 ; fi

INTERFACE=$(netstat -rn|head -n3|grep 0.0.0.0|awk {'print $8'})
IP=$(ifconfig "${INTERFACE}" | grep "inet addr" | cut -d ':' -f 2 | cut -d ' ' -f 1)

MASTER="url.default.master"

echo "Cleaning..." 
docker rm -f "$(docker ps -qa)"

echo "Redis type: "$1" - Master: "${MASTER}""
echo "Running Redis Instance..."

if [ "$1" == "master" ]; then 
	echo "Run redis-server MASTER"
	if [ "$2" == "net" ]; then
		docker run -d --name redis-master --net=host redis:4
	else
		docker run -d --name redis-master -p "${IP}":6379:6379 redis:4 
	fi	
fi

if [ "$1" == "slave" ]; then 
	echo "Run redis-server SLAVE"
	if [ "$2" == "net" ]; then	
		docker run -d --name redis-slave --net=host redis:4 
	else
		docker run -d --name redis-slave -p "${IP}":6379:6379 redis:4
	fi
	docker exec -ti redis-slave redis-cli -h "${IP}" -p 6379 SLAVEOF "${MASTER}" 6379
fi

echo "Running Redis Sentinel..."
docker build -t sentinel-"$1" .
if [ "$2" == "net" ]; then
	docker run -d --name sentinel-"$1" --net=host -e QUORUM=2 -e MASTER="${MASTER}" -e MASTER_NAME=redis-cluster sentinel-"$1"
else
	docker run -d --name sentinel-"$1" -p 26379:26379 -e QUORUM=2 -e MASTER="${MASTER}" -e MASTER_NAME=redis-cluster -e ANNOUNCE_IP="${IP}" sentinel-"$1"
fi
docker logs -f sentinel-"$1"


