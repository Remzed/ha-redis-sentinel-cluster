# High-Availability Redis/Sentinel cluster 


## Workflow
- Application send request to localhost:6379 (default)
- Haproxy intercept request .
- Haproxy check which backend return master by tcp-check.
- Haproxy check availability for each redis instance.
- Haproxy check if sentinels are agree on the actual master in the cluster.
- Haproxy forward the request to the redis master instance.
- If a redis-master instance is not working as expected, Sentinel can start a failover process where a slave is promoted to master, the other additional slaves are reconfigured to use the new master.
- Each redis instance is a slaveof the actual redis master.

## Demo

### Setup

Run `start.sh` with master/slave argument.

We run 1 node master and 2 nodes slaves.

#### Master

``` ./start.sh master ``` will run 

- default redis-server "master" instance 
- redis-sentinel monitoring redis-server "master" instance

##### Under the hood
``` 
docker run -d --name redis-master --net=host redis:4 

docker run -d --name sentinel-master --net=host 
-e QUORUM=2 
-e MASTER=URL_MASTER 
-e MASTER_NAME=redis-cluster 
sentinel-"$1"
```


#### Slave

``` ./start.sh slave ``` will run

- redis-server "slaveof master" instance 
- redis-sentinel monitoring redis-server "master" instance

##### Under the hood
```
docker run -d --name redis-slave --net=host redis:4
docker exec -ti redis-slave 
redis-cli -p 6379 SLAVEOF URL_MASTER 6379

docker run -d --name sentinel-master --net=host 
-e QUORUM=2 
-e MASTER=URL_MASTER 
-e MASTER_NAME=redis-cluster 
sentinel-"$1"
```

#### Loadbalancer
`/etc/haproxy/haproxy.cfg/`

```
# Specifies listening socket for accepting client connections using the default
# REDIS TCP timeout and backend bk_redis TCP health check.
frontend front_redis
  bind 127.0.0.1:6379 name redis
# If at least 2 sentinels agree with the redis host that it is master, use it.
  use_backend bk_redis-a if { srv_is_up(bk_redis-a/redis-a) } { nbsrv(bk_chk_master_redis-a) ge 2 }
  use_backend bk_redis-b if { srv_is_up(bk_redis-b/redis-b) } { nbsrv(bk_chk_master_redis-b) ge 2 }
  use_backend bk_redis-c if { srv_is_up(bk_redis-c/redis-c) } { nbsrv(bk_chk_master_redis-c) ge 2 }
  

# Specifies the backend Redis proxy server TCP health settings
# Ensure it only forward incoming connections to reach a master.
backend bk_redis-a
  option tcp-check
  tcp-check connect
  tcp-check send PING\r\n
  tcp-check expect string +PONG
  tcp-check send info\ replication\r\n
  tcp-check expect string role:master
  tcp-check send QUIT\r\n
  tcp-check expect string +OK
  server redis-a redis-a:6379 check inter 1s


backend bk_redis-b
  option log-health-checks
  option tcp-check
  tcp-check connect
  tcp-check send PING\r\n
  tcp-check expect string +PONG
  tcp-check send info\ replication\r\n
  tcp-check expect string role:master
  tcp-check send QUIT\r\n
  tcp-check expect string +OK
  server redis-b redis-b:6379 check inter 1s


backend bk_redis-c
  option log-health-checks
  option tcp-check
  tcp-check connect
  tcp-check send PING\r\n
  tcp-check expect string +PONG
  tcp-check send info\ replication\r\n
  tcp-check expect string role:master
  tcp-check send QUIT\r\n
  tcp-check expect string +OK
  server redis-b redis-c:6379 check inter 1s

# Check 3 sentinels to see if they think redis-a is master
backend bk_chk_master_redis-a
  option log-health-checks
  log /dev/log  local4
  option tcp-check
  tcp-check connect
  tcp-check send PING\r\n
  tcp-check expect string +PONG
  tcp-check send SENTINEL\ get-master-addr-by-name\ redis-cluster\r\n
  tcp-check expect string IP.OF.REDIS-A
  tcp-check send QUIT\r\n
  tcp-check expect string +OK
  server redis-a-sentinel redis-a:26379 check inter 1s
  server redis-b-sentinel redis-b:26379 check inter 1s
  server redis-b-sentinel redis-c:26379 check inter 1s

# Check 3 sentinels to see if they think redis-b is master
backend bk_chk_master_redis-b
  option log-health-checks
  log /dev/log  local5
  option tcp-check
  tcp-check connect
  tcp-check send PING\r\n
  tcp-check expect string +PONG
  tcp-check send SENTINEL\ get-master-addr-by-name\ redis-cluster\r\n
  tcp-check expect string IP.OF.REDIS-B
  tcp-check send QUIT\r\n
  tcp-check expect string +OK
  server redis-a-sentinel redis-a:26379 check inter 1s
  server redis-b-sentinel redis-b:26379 check inter 1s
  server redis-b-sentinel redis-c:26379 check inter 1s
  
# Check 3 sentinels to see if they think redis-b is master
backend bk_chk_master_redis-c
  option log-health-checks
  log /dev/log  local5
  option tcp-check
  tcp-check connect
  tcp-check send PING\r\n
  tcp-check expect string +PONG
  tcp-check send SENTINEL\ get-master-addr-by-name\ redis-cluster\r\n
  tcp-check expect string IP.OF.REDIS-C
  tcp-check send QUIT\r\n
  tcp-check expect string +OK
  server redis-a-sentinel redis-a:26379 check inter 1s
  server redis-b-sentinel redis-b:26379 check inter 1s  
  server redis-b-sentinel redis-c:26379 check inter 1s

```

### Testing
``` 
user@client:[~]: redis-cli 
127.0.0.1:6379> info replication
# Replication
role:master
connected_slaves:2
slave0:ip=IP.OF.REDIS-B,port=6379,state=online,offset=330548,lag=0
slave1:ip=IP.OF.REDIS.C,port=6379,state=online,offset=330548,lag=0
master_replid:08b567526bb7406462a47920a7933de5aa95082c
master_replid2:05c6afb44ba59e0d836b5e0396246fcabb66c61b
master_repl_offset:330548
second_repl_offset:25096
repl_backlog_active:1
repl_backlog_size:1048576
repl_backlog_first_byte_offset:1187
repl_backlog_histlen:329362
```

We are connected to the redis master instance, we have 1 connected slave.

To check who is master, query one of the sentinel :

```
user@client:[~]: redis-cli -h redis-a -p 26379 SENTINEL get-master-addr-by-name redis-cluster

1) "IP.OF.REDIS-A"
2) "6379"
```

##### Testing the failover

###### From the redis client

```
user@client:[~]: redis-cli 
127.0.0.1:6379> set key_shared "value_shared"
OK
127.0.0.1:6379> get key_shared
"value_shared"
```

###### Kill the master

```
user@redis-master:[~]: docker stop redis sentinel
```

###### Check the other sentinels logs

```
# +sdown sentinel 66e3aa4599b3dc8ee258fb162e7abd66195609fb IP.OF.REDIS-A 26379 @ redis-cluster IP.OF.REDIS-A 6379
# +sdown master redis-cluster IP.OF.REDIS-A 6379
# +odown master redis-cluster IP.OF.REDIS-A 6379 #quorum 2/2
# +new-epoch 2
# +try-failover master redis-cluster IP.OF.REDIS-A 6379
# +vote-for-leader 684a183d08c1757c8fa59868ee2615ac09bdc5c4 2
# bc17766acb0028dcd2995ba32d601485128c983e voted for 684a183d08c1757c8fa59868ee2615ac09bdc5c4 2
# -failover-abort-not-elected master redis-cluster IP.OF.REDIS-A 6379
# Next failover delay: I will not start a failover before Fri Mar 2 15:42:03 2018

# +new-epoch 3
# +try-failover master redis-cluster IP.OF.REDIS-A 6379
# +vote-for-leader bc17766acb0028dcd2995ba32d601485128c983e 3
# 684a183d08c1757c8fa59868ee2615ac09bdc5c4 voted for bc17766acb0028dcd2995ba32d601485128c983e 3
# +elected-leader master redis-cluster IP.OF.REDIS-A 6379
# +failover-state-select-slave master redis-cluster IP.OF.REDIS-A 6379
# +selected-slave slave IP.OF.REDIS-C:6379 IP.OF.REDIS-C 6379 @ redis-cluster IP.OF.REDIS-A 6379
* +failover-state-send-slaveof-noone slave IP.OF.REDIS-C:6379 IP.OF.REDIS-C 6379 @ redis-cluster IP.OF.REDIS-A 6379
* +failover-state-wait-promotion slave IP.OF.REDIS-C:6379 IP.OF.REDIS-C 6379 @ redis-cluster IP.OF.REDIS-A 6379
# +promoted-slave slave IP.OF.REDIS-C:6379 IP.OF.REDIS-C 6379 @ redis-cluster IP.OF.REDIS-A 6379
# +failover-state-reconf-slaves master redis-cluster IP.OF.REDIS-A 6379
* +slave-reconf-sent slave IP.OF.REDIS.B:6379 IP.OF.REDIS.B 6379 @ redis-cluster IP.OF.REDIS-A 6379
* +slave-reconf-inprog slave IP.OF.REDIS.B:6379 IP.OF.REDIS.B 6379 @ redis-cluster IP.OF.REDIS-A 6379
# -odown master redis-cluster IP.OF.REDIS-A 6379
* +slave-reconf-done slave IP.OF.REDIS.B:6379 IP.OF.REDIS.B 6379 @ redis-cluster IP.OF.REDIS-A 6379
# +failover-end master redis-cluster IP.OF.REDIS-A 6379
# +switch-master redis-cluster IP.OF.REDIS-A 6379 IP.OF.REDIS-C 6379
* +slave slave IP.OF.REDIS.B:6379 IP.OF.REDIS.B 6379 @ redis-cluster IP.OF.REDIS-C 6379
* +slave slave IP.OF.REDIS-A:6379 IP.OF.REDIS-A 6379 @ redis-cluster IP.OF.REDIS-C 6379
```

###### The same comportements can be observed with haproxy.log

```
load-alpha haproxy[12026]: Server bk_redis/redis-a is DOWN, reason: Layer4 connection problem, info: "Connection refused at step 1 of tcp-check (connect)", check duration: 0ms. 0 active and 0 backup servers left. 0 sessions active, 0 requeued, 0 remaining in queue.
load-alpha haproxy[12026]: backend bk_redis has no server available!
load-alpha haproxy[12026]: Server bk_redis/redis-c is UP, reason: Layer7 check passed, code: 0, info: "(tcp-check)", check duration: 0ms. 1 active and 0 backup servers online. 0 sessions requeued, 0 total in queue.
```

###### Check who is master 

```
user@client:[~]: redis-cli -h redis-a -p 26379 SENTINEL get-master-addr-by-name redis-cluster
1) "IP.OF.REDIS-C"
2) "6379"
```

###### Check if data replication still works
```
user@client:[~]: redis-cli GET key_shared
"value_shared"
```

## Environment Variables
`MASTER` - Colon-separated IP address and port or Redis master. Port is optional, `REDIS_PORT` is used when missing. E.g. `ip_address` or `ip_address:port`.

`REDIS_PORT` - Port on which is master available. Default value is `6379`.

`SENTINEL_PORT` - A port on which sentinel is communicating. A default value is `26379`. In case you change this value, don't forget to expose additional port manually. By default, only `26379` is exposed.

`MASTER_NAME` - Unique name for master. When defined, monitoring will be initialized.

`QUORUM` - Number of Sentinels that need to agree about the fact the master is not reachable, in order for really mark the slave as failing, and eventually start a fail over procedure if possible. Default value is `2`.

`DOWN_AFTER` - Time in milliseconds an instance should not be reachable for a Sentinel starting to think it is down. Default value `30000`.

`FAILOVER_TIMEOUT` - Wait time before failover retry of the same master. Default value `180000`.

`PARALLEL_SYNCS` - Sets the number of slaves that can be reconfigured to use the new master after a failover at the same time. Default value `1`.

`SLAVES` - Manually setting of all the slaves of monitored master. Accepted format is a colon-separated IP address and port for each slave server. Multiple slaves are separated by a semicolon. E.g. `ip_address:host;ip_address`.

`NOTIFICATION_SCRIPT` - Manually setting of notification-script on master. Script must exists and be executable or container will fail to start.

`CLIENT_RECONFIG_SCRIPT` - Manually setting of client-reconfig-script on master. Script must exists and be executable or container will fail to start.

`ANNOUNCE_IP` - Host machine IP address.

`ANNOUNCE_PORT` - Mapped sentinel port.

`AUTH_PASS` - Authentication password to use when connecting to master.